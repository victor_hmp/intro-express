import { Router } from 'express';
import { connectToMongo } from './db';

/**
 * There multiple calls to connectToMongo being performed in this router,
 * but according to
 * https://mongodb.github.io/node-mongodb-native/3.1/api/MongoClient.html#db
 * once a connection to a DB is created, it is cached, so performing
 * these calls multiple times would always return the same instance.
 */
const projectsRouter = Router();

const findProject = async (req, res, next) => {
  const db = await connectToMongo();
  const { slug } = req.params;

  const project = await db.collection('projects').findOne({ slug });

  if (!project) {
    res.status(400).json({ error: `project ${slug} does not exist` });
  } else {
    req.body.project = project;
    next();
  }
};

projectsRouter.post('/', async (req, res) => {
  const { name } = req.body;
  const db = await connectToMongo();

  const slug = name.toLowerCase();

  const project = {
    slug,
    boards: [],
  };

  const existingProject = await db.collection('projects').findOne({ slug });

  if (existingProject) {
    res.status(400).json({ error: `project ${slug} already exists` });
  } else {
    await db.collection('projects').insertOne(project);
    res.json({ project });
  }
});

projectsRouter.get('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  res.json({ project });
});

projectsRouter.delete('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  const db = await connectToMongo();

  const foundSlug = project.slug;

  try {
    const deleteResult = await db
      .collection('projects')
      .deleteOne({ slug: foundSlug });

    if (deleteResult.result.ok === 1 && deleteResult.deletedCount > 0) {
      res.json({ message: `Deleted ${foundSlug} project.` });
    } else {
      res.json({ message: 'Project was not deleted.' });
    }
  } catch (err) {
    res.status(500).json({ message: 'Something went wrong.', error: err });
  }
});

projectsRouter.post('/:slug/boards', findProject, async (req, res) => {
  const { name, project } = req.body;
  const db = await connectToMongo();

  const board = { name, tasks: [] };
  project.boards.push(board);

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ board });
});

projectsRouter.delete('/:slug/boards/:name', findProject, async (req, res) => {
  const { project } = req.body;
  const { name: boardName } = req.params;
  const db = await connectToMongo();

  const board = await db
    .collection('projects')
    .findOne({ 'boards.name': boardName });

  if (!board) {
    res.status(400).json({
      error: `Project ${project.slug} does not contain a board named ${boardName}.`,
    });
  } else {
    project.boards = project.boards.filter((board) => board.name !== boardName);

    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);

    res.json({ project });
  }
});

projectsRouter.post(
  '/:slug/boards/:name/tasks',
  findProject,
  async (req, res) => {
    const { project, ...taskFromRequestBody } = req.body;
    const { name: boardName } = req.params;
    const db = await connectToMongo();

    const board = await db
      .collection('projects')
      .findOne({ 'boards.name': boardName });

    if (!board) {
      res.status(400).json({
        error: `Project ${project.slug} does not contain a board named ${boardName}.`,
      });
    } else {
      const boardIndex = project.boards.findIndex(
        (board) => board.name === boardName
      );
      project.boards[boardIndex].tasks.push(taskFromRequestBody);

      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

      res.json({ board: project.boards[boardIndex] });
    }
  }
);

projectsRouter.delete(
  '/:slug/boards/:name/tasks/:id',
  findProject,
  async (req, res) => {
    const { project } = req.body;
    const { name: boardName, id: taskToDeleteId } = req.params;
    const db = await connectToMongo();

    const board = await db
      .collection('projects')
      .findOne({ 'boards.name': boardName });

    if (!board) {
      res.status(400).json({
        error: `Project ${project.slug} does not contain a board named ${boardName}.`,
      });
    } else {
      const boardIndex = project.boards.findIndex(
        (board) => board.name === boardName
      );

      project.boards[boardIndex].tasks = project.boards[
        boardIndex
      ].tasks.filter((task) => task.id !== Number(taskToDeleteId));

      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

      res.json({ board: project.boards[boardIndex] });
    }
  }
);

export default projectsRouter;
