import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { connectToMongo } from './db';
import projectsRouter from './projects';

export const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.disable('x-powered-by');

export async function bootstrap() {
  const handler = (_, res) => {
    res.json({ message: 'hello world' });
  };

  app.get('/', handler);

  app.use('/projects', projectsRouter);

  const db = await connectToMongo();

  return db;
}
