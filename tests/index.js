import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('Projects Router', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  test('POST /projects/:slug/boards/:name/tasks adds a new task into a board', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const {
      body: { board },
    } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name: 'todo' });

    const task = {
      id: 123,
      title: 'A test task',
      description: 'Run some tests and make sure the API is working.',
    };

    const { body: postBodyResponse } = await request(app)
      .post(`/projects/${slug}/boards/${board.name}/tasks`)
      .send(task);

    expect(postBodyResponse).toBeDefined();
    expect(postBodyResponse.board.tasks).toContainEqual(task);
  });

  test('DELETE /projects/:slug deletes a project', async () => {
    const projectName = 'Awesome';

    const projectNotFoundError = {
      error: `project awesome does not exist`,
    };

    const project = { name: projectName };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const { body, status } = await request(app)
      .delete(`/projects/${slug}`)
      .send({ name: projectName });

    expect(status).toEqual(200);
    expect(body.message).toBe(`Deleted ${slug} project.`);

    const { status: getStatus, body: getBody } = await request(app).get(
      `/projects/${slug}`
    );

    expect(getStatus).toBe(400);
    expect(getBody).toEqual(projectNotFoundError);
  });

  test('DELETE /projects/:slug/boards/:name deletes a board', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const {
      body: { board: createdBoard },
    } = await request(app).post(`/projects/${slug}/boards`).send({ name });

    const { body, status } = await request(app).delete(
      `/projects/${slug}/boards/${createdBoard.name}`
    );

    expect(status).toEqual(200);
    expect(body.project.boards).not.toContain(createdBoard);
  });

  test('DELETE /projects/:slug/boards/name/tasks/:id deletes a task from a board', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const {
      body: { board },
    } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name: 'todo' });

    const task = {
      id: 123,
      title: 'A test task',
      description: 'Run some tests and make sure the API is working.',
    };

    await request(app)
      .post(`/projects/${slug}/boards/${board.name}/tasks`)
      .send(task);

    const taskToDelete = 123;

    const { body: deleteBodyResponse } = await request(app).delete(
      `/projects/${slug}/boards/${board.name}/tasks/${taskToDelete}`
    );

    expect(deleteBodyResponse.board.tasks).not.toContainEqual(task);
  });
});

describe('Base App', () => {
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });
});
